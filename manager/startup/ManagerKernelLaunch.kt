package com.project.example.apps.manager.startup

import android.content.Context
import com.project.example.common.core.directories.PrivateFilesDirectory
import com.project.example.common.core.directories.SharedFilesDirectory
import com.project.example.manager.ManagerKernelLoopV1
import com.project.example.directories.existsFile
import com.project.example.hardware.wizarpos.WizarposBitmapReceiptPrinter
import com.project.example.kernel.common.KernelDialogs
import com.project.example.kernel.dialogs.DvDialogs
import com.project.example.kernel.dialogs.DvDialogsListener
import com.project.example.kernel.receipt.DvKernelReceipt
import com.project.example.kernel_log_console.KernelLogConsole

class ManagerKernelLaunch(
        private val context: Context,
        private val dvDialogsListener: DvDialogsListener,
        private val idleAction: () -> Unit
) : ManagerStartAction {
    private val privateDir = PrivateFilesDirectory(context)
    private val sharedDir = SharedFilesDirectory(context)
    private lateinit var loop: ManagerKernelLoopV1
    private lateinit var dialogs: KernelDialogs


    override fun run() {
        if (checkParameters()) {
            initKernelThread()
        } else {
            throw IllegalStateException()
        }
    }

    private fun initKernelThread() {
        Thread(ThreadGroup("Lib"), {
            launchKernelLoop()
            launchKernelDialogs()
            launchKernelReceipt()

            loop.init()
            loop.idle()
        }, "LibMainLoop").start()
    }

    private fun launchKernelLoop() {
        loop = ManagerKernelLoopV1(
                privateDir.path,
                sharedDir.path,
                idleAction
        )
    }

    private fun launchKernelDialogs() {
        dialogs = DvDialogs(context, dvDialogsListener)
    }

    private fun launchKernelReceipt() {
        DvKernelReceipt(
                context,
                WizarposBitmapReceiptPrinter(context),
                KernelLogConsole("DvKernelReceipt")
        )
    }

    private fun checkParameters() = privateDir.existsFile("DVCoreAdParm.xml")
}