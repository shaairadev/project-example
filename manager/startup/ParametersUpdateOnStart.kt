package com.project.example.apps.manager.startup

import android.content.Context
import com.project.example.common.core.directories.ParameterFilesDirectory
import com.project.example.common.core.directories.PrivateFilesDirectory
import com.project.example.common.core.directories.TransferFilesDirectory
import com.project.example.common.core.transfer.copy.DirectoryCopy
import com.project.example.common.core.transfer.copy.DirectoryCopyAndDelete
import com.project.example.manager.ApplicationParameterUpdater
import com.project.example.manager.services.management.ManualConnectionsPool
import com.dejavoo.terwin.OnMainThread

class ParametersUpdateOnStart(
        context: Context,
        private val connections: ManualConnectionsPool
) : ManagerContextAction(context) {
    private val parametersDir = ParameterFilesDirectory(context)
    private val privateDir = PrivateFilesDirectory(context)

    override fun run() {
        if (DirectoryCopy(TransferFilesDirectory().path, parametersDir.path).notAvailable()) {
            return
        }

        saveParametersForYourself()
        saveParametersForOthersAndClear()
        pushParameterUpdates(ParameterFilesDirectory(context).path)

    }

    private fun saveParametersForYourself() =
            DirectoryCopy(
                    TransferFilesDirectory().path,
                    privateDir.path
            ).make()


    private fun saveParametersForOthersAndClear() =
            DirectoryCopyAndDelete(
                    TransferFilesDirectory().path,
                    parametersDir.path
            ).make()


    private fun pushParameterUpdates(filePath: String) {
        OnMainThread {
            connections.get()
                    .forEach {
                        ApplicationParameterUpdater(filePath, it).update(context)
                    }
        }.execute()
    }
}