package com.project.example.manager.startup

import android.content.Context


interface ManagerStartAction {
    fun run()
}

abstract class ManagerContextAction(protected val context: Context) : ManagerStartAction