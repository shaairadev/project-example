package com.project.example.manager.services

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.IBinder
import com.project.example.apps.common.core.Command
import com.project.example.client_service.AppService
import com.project.example.kernel.common.KernelLog
import java.util.concurrent.PriorityBlockingQueue

abstract class ApplicationServiceConnection(
        private val context: Context,
        private val logger: KernelLog
) : ApplicationConnection {
    abstract val action: String
    abstract val applicationPackage: String
    private val commands = PriorityBlockingQueue<Command>()

    private var service: AppService? = null

    override fun onServiceConnected(name: ComponentName, boundService: IBinder) {
        service = AppService.Stub.asInterface(boundService)
        while (commands.isNotEmpty()) {
            val executable = commands.take()
            executable.execute()
        }
    }

    override fun onServiceDisconnected(name: ComponentName) {
        service = null
        open()
    }

    override fun open() {
        logger.log("Opening connection to '$applicationPackage'")
        val i = Intent(action)
        i.`package` = applicationPackage
        context.bindService(i, this, Context.BIND_AUTO_CREATE)
    }

    override fun close() {
        logger.log("Closing connection to '$applicationPackage'")
        context.unbindService(this)
    }

    override fun updateApplicationParameters(context: Context, uris: List<Uri>) {
        val command = UpdateCommand(uris, context)
        if (service != null) {
            command.execute()
        } else {
            commands.add(command)
        }
    }

    inner class UpdateCommand(private val uris: List<Uri>, private val context: Context) : Command {
        override fun execute() {
            giveUriPermissions()
            service?.updateApplicationParameters(uris)
        }

        private fun giveUriPermissions() {
            for (uri in uris) {
                context.grantUriPermission(
                        applicationPackage,
                        uri,
                        Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_READ_URI_PERMISSION
                )
            }
        }
    }
}