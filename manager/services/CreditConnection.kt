package com.project.example.manager.services

import android.content.Context
import com.project.example.kernel.common.KernelLog


class CreditConnection(context: Context, logger: KernelLog) : ApplicationServiceConnection(context, logger) {
    override val action = "com.project.example.credit.AppService"
    override val applicationPackage = "com.project.example.credit"
}