package com.project.example.manager.services.management

import android.content.Context
import com.project.example.manager.services.ApplicationConnection

open class ManualConnectionsPool(context: Context) : ManagerApplicationConnectionsPool(context) {
    override fun open() {
        giftConnection.open()
        creditConnection.open()
    }

    override fun close() {
        giftConnection.close()
        creditConnection.close()
    }

    override fun get(): Iterable<ApplicationConnection> = listOf(giftConnection, creditConnection)
}