package com.project.example.manager.services.management

import android.content.Context
import com.project.example.manager.services.ApplicationConnection
import com.project.example.manager.services.CreditConnection
import com.project.example.manager.services.GiftConnection
import com.project.example.kernel_log_console.KernelLogConsole

abstract class ManagerApplicationConnectionsPool(context: Context) : ApplicationConnectionsPool {
    protected val giftConnection: ApplicationConnection = GiftConnection(context, KernelLogConsole("GiftConnection"))
    protected val creditConnection: ApplicationConnection = CreditConnection(context, KernelLogConsole("CreditConnection"))
}