package com.project.example.manager.services.management

import android.content.Context

class AutoLaunchingConnectionsPool(context: Context) : ManualConnectionsPool(context) {
    init {
        open()
    }
}