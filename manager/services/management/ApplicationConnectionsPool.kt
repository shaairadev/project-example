package com.project.example.manager.services.management

import com.project.example.manager.services.ApplicationConnection

interface ApplicationConnectionsPool {
    fun open()

    fun close()

    fun get(): Iterable<ApplicationConnection>
}


