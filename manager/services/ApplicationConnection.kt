package com.project.example.manager.services

import android.content.Context
import android.content.ServiceConnection
import android.net.Uri

interface ApplicationConnection : ServiceConnection {
    fun open()
    fun close()
    fun updateApplicationParameters(context: Context, uris: List<Uri>)
}