package com.project.example.manager.services

import android.content.Context
import com.project.example.kernel.common.KernelLog


class GiftConnection(context: Context, logger: KernelLog) : ApplicationServiceConnection(context, logger) {
    override val action = "com.project.example.gift.AppService"
    override val applicationPackage = "com.project.example.gift"
}