package com.project.example.manager

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.animation.Animation
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.project.example.app.R
import com.project.example.apps.common.AppActivity
import com.project.example.apps.common.core.RemoveOldLogs
import com.project.example.apps.common.core.TransferAssets
import com.project.example.apps.common.core.directories.PrivateFilesDirectory
import com.project.example.apps.common.screens.SplashScreen
import com.project.example.apps.manager.services.management.AutoLaunchingConnectionsPool
import com.project.example.apps.manager.startup.ManagerKernelLaunch
import com.project.example.apps.manager.startup.ParametersUpdateOnStart
import com.project.example.hardware.common.PinPadDevice
import com.project.example.kernel.commands.SDKCommandsApi
import com.project.example.kernel.common.KernelWindow
import com.project.example.kernel.dialogs.DvDialogsListener
import com.project.example.kernel.spin.InternalSpin
import com.project.example.kernel.window.KernelWindowCallback
import com.project.example.kernel.window.KernelWindowV1
import com.project.example.kernel_log_console.KernelLogConsole
import com.project.example.screens.*
import com.project.example.sdk.pinpad.PinPadFactory
import com.project.example.shared.common.commands.CommandsApi

private const val REQUEST_PERMISSION = 101

/*
Это приложение-менеджер, занимающееся поиском, обновлением, запуском и рассылкой различных команд
семейству других установленных приложений. Большая часть presentation слоя здесь удалена.
Все построено на одном Activity и множестве экранов-View, запускаемых на уровне линуксового NDK кода
(требование сферы)
 */

class ManagerActivity : AppActivity(), WindowFragment.WindowListener {
    private lateinit var window: KernelWindow
    private lateinit var navigation: Navigation

    private lateinit var commandsApi: CommandsApi
    private lateinit var pinPadDevice: PinPadDevice
    private lateinit var spin: InternalSpin

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.single_activity)

        commandsApi = SDKCommandsApi(this)
        pinPadDevice = PinPadFactory.resolve(this)
        spin = InternalSpin()

        pinPadDevice.setTheme(PinPadTheme(this).theme)
        pinPadDevice.setLayout(PinPadLayout(this).layout)

        TransferAssets(assets, PrivateFilesDirectory(this)).transfer()
        RemoveOldLogs(PrivateFilesDirectory(this).path).run()

        ParametersUpdateOnStart(this, AutoLaunchingConnectionsPool(this)).run()

        try {
            ManagerKernelLaunch(this, buildDialogsListener()) { openIdle() }.run()
            createKernelWindow()

        } catch (error: IllegalStateException) {
            navigation.replace(UpdateParametersScreen())
        }
    }
}