package com.project.example.apps.manager

import com.project.example.kernel.common.KernelLoop
import com.project.example.screens.IdlePaymentTypesScreen
import com.project.example.Navigation
import com.project.example.OnMainThread


class ManagerKernelLoopV1(
        path: String,
        sharedPath: String,
        private val onShowIdle: () -> Unit
) : KernelLoop {
    companion object {
        init {
            System.loadLibrary("DvExample")
        }
    }

    init {
        nativePrepareContext()
        nativeSetPath(path)
        nativeSetSharedPath(sharedPath)
    }

    override fun init() {
        nativeInit()
    }

    override fun idle() {
        nativeIdle()
    }

    fun askPassword(onSuccess: () -> Unit) {
        Thread {
            if (nativeAskPassword()) {
                OnMainThread {
                    onSuccess()
                }.execute()
            }
        }
    }

    //region Native calls
    private fun ShowIdle() {
        onShowIdle()
    }
    //endregion

    private external fun nativePrepareContext()
    private external fun nativeInit()
    private external fun nativeIdle()
    private external fun nativeSetPath(path: String)
    private external fun nativeSetSharedPath(path: String)

    private external fun nativeAskPassword(): Boolean

}