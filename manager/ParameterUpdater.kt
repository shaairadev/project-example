package com.project.example.apps.manager

import android.content.Context
import android.net.Uri
import android.util.Log
import androidx.core.content.FileProvider
import com.project.example.manager.services.ApplicationConnection
import java.io.File

interface ParameterUpdater {
    fun update(context: Context)
}

class ApplicationParameterUpdater(
        private val parametersPath: String,
        private val connection: ApplicationConnection
) : ParameterUpdater {
    private val providerPackage: String = "com.project.example.provider"

    override fun update(context: Context) {
        val directory = File(parametersPath)

        if (!directory.exists() || !directory.isDirectory) {
            return
        }

        val uris = mutableListOf<Uri>()

        directory.walkTopDown()
                .filter { it.isFile }
                .forEach {
                    val uri = FileProvider.getUriForFile(context, providerPackage, it)
                    uris.add(uri)
                }

        connection.updateApplicationParameters(context, uris)
    }


}