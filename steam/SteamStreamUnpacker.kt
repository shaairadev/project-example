package com.dejavoo.aura.steam

import android.util.Log
import com.dejavoo.aura.steam.archives.TarSteamArchive
import java.io.InputStream
import java.util.zip.GZIPInputStream

interface SteamStreamUnpacker {
    fun execute(stream: InputStream): List<SteamFile>
}

class SteamParametersCompleteUnpacker(private val path: String) : SteamStreamUnpacker {
    override fun execute(stream: InputStream): List<SteamFile> {
        val topLevelFiles = TarSteamArchive(stream, path).unpack()

        val tarArchives = topLevelFiles.filter { TarArchiveFilter().accept(it) }

        val gzipArchives = mutableListOf<SteamFile>()
        val lowLevelFiles = mutableListOf<SteamFile>()

        tarArchives.forEach { gzipArchives.addAll(getGZipFiles(it)) }

        gzipArchives.forEach { lowLevelFiles.addAll(getLowLevelFiles(it)) }

        return lowLevelFiles
    }

    private fun getGZipFiles(tarArchive: SteamFile): List<SteamFile> {
        val filesFromTar = TarSteamArchive(tarArchive.stream(), path).unpack()
        return filesFromTar.filter { GZipArchiveFilter().accept(it) }
    }


    private fun getLowLevelFiles(gzipArchive: SteamFile): List<SteamFile> {
        return TarSteamArchive(GZIPInputStream(gzipArchive.stream()), path).unpack()
    }
}