package com.dejavoo.aura.steam

interface ParametersUpdate {
    fun update(tpn: String)
}