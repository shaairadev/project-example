package com.dejavoo.aura.steam

import android.content.Context
import com.dejavoo.aura.app.R
import java.io.IOException
import java.net.SocketException

class SteamTpnNotFoundException : Throwable()
class SteamTpnIsBlockedException : Throwable()

class SteamException : Throwable()

class SteamErrorsFactory(
        private val context: Context
) : SteamErrors {

    override fun getErrorMessage(t: Throwable): String {
        return when (t) {
            is SteamTpnNotFoundException -> context.getString(R.string.errors_steam_tpn_not_found)
            is SteamTpnIsBlockedException -> "TPN is blocked"
            is IOException -> "Copying parameters failed"
//            is SocketException -> context.getString(R.string.errors_steam_not_connected)
//            is SteamException -> context.getString(R.string.errors_steam_unknown)
            else -> context.getString(R.string.errors_steam_not_connected)
        }
    }
}

interface SteamErrors {
    fun getErrorMessage(t: Throwable): String
}