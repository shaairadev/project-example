package com.dejavoo.aura.steam

interface SteamFileFilter {
    fun accept(steamFile: SteamFile): Boolean
}

class TarArchiveFilter : SteamFileFilter {
    override fun accept(steamFile: SteamFile): Boolean {
        return steamFile.fileName.endsWith(".tar") && steamFile.fileName.startsWith("D.")
    }
}

class GZipArchiveFilter : SteamFileFilter {
    override fun accept(steamFile: SteamFile): Boolean {
        return steamFile.fileName.endsWith(".tar.gz")
    }
}

class XMLFilter : SteamFileFilter {
    override fun accept(steamFile: SteamFile): Boolean {
        return steamFile.fileName.endsWith(".xml")
    }
}

class JSONFilter : SteamFileFilter {
    override fun accept(steamFile: SteamFile): Boolean {
        return steamFile.fileName.endsWith(".json")
    }
}
