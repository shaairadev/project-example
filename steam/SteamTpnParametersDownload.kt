package com.dejavoo.aura.steam

import java.io.InputStream

interface SteamDownload {
    fun stream(): InputStream
}

class SteamTpnParametersDownload(
        private val tpn: String
) : SteamDownload {
    override fun stream(): InputStream {
        return WebParametersByTpnSteamFile(tpn).stream()
    }
}