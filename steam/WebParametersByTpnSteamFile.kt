package com.dejavoo.aura.steam

import android.net.Uri
import android.os.Build
import android.util.Log
import com.dejavoo.aura.apps.common.core.SteamServer
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import java.io.*

class WebParametersByTpnSteamFile(
    private val tpn: String,
    private val httpClient: OkHttpClient = OkHttpClient(),
    private val steamServer: SteamServer = SteamServer()
): SteamFile {

    override val fileName: String
        get() = "$tpn.tar"

    override val filePath: String
        get() = Uri.parse(steamServer.url)
                .buildUpon()
                .appendQueryParameter("tpn", tpn)
                .appendQueryParameter("termSerNo", Build.SERIAL)
//                .appendQueryParameter("SID", String.valueOf(DateUtility.getCurrentTimestamp().getTime()))
//                .appendQueryParameter("DeviceID", Build.DEVICE)
//                .appendQueryParameter("DeviceModel", "Dejavoo Z11")
//                .appendQueryParameter("Hardware", "Dejavoo Z11")
                .appendQueryParameter("type", "Partial")
                .appendQueryParameter("version", "2")
                .appendQueryParameter("network", "WiFi")
//                .appendQueryParameter("AppVersion", BuildConfig.VERSION_NAME)
//                .appendQueryParameter("Build", BuildConfig.VERSION_CODE.toString())
                .build()
                .toString()


    override fun stream(): InputStream {
        val request = Request.Builder()
                .url(filePath)
                .get()
                .build()

        val response: Response = httpClient.newCall(request).execute()

        if (response.isSuccessful) {
            val bytes = response.body?.bytes() ?: throw Exception("failed fetch")

            Log.e("WebParametersByTpn", "Received ${bytes.size} bytes")

            if (bytes.contentToString().contains("Terminal is locked", ignoreCase = true)) {
                throw SteamTpnIsBlockedException()
            }

            return ByteArrayInputStream(bytes)
        } else {
            if (response.code == 404) {
                throw SteamTpnNotFoundException()
            } else {
                throw Exception("Request failed")
            }
        }
    }
}