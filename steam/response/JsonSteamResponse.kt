package com.dejavoo.aura.steam.response

import com.dejavoo.aura.steam.response.ISteamResponse
import org.json.JSONObject

class JsonSteamResponse(private val json: JSONObject) : ISteamResponse {
    override val path: String
        get() = json.getString("Path")
    override val code: Int
        get() = json.getInt("Code")
    override val errorMessage: String
        get() = json.getString("ErrMessage")
}