package com.dejavoo.aura.steam.response

import com.dejavoo.aura.XmlDocument

class XmlSteamResponse(private val xml: XmlDocument) : ISteamResponse {
    override val path: String
        get() = xml.getFieldValueOrDefault("Path", "")
    override val code: Int
        get() = xml.getFieldIntValue("Code", -1)
    override val errorMessage: String
        get() = xml.getFieldValueOrDefault("ErrMessage", "")
}