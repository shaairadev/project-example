package com.dejavoo.aura.steam.response

interface ISteamResponse {
    val path: String
    val code: Int

    val errorMessage: String
}