package com.dejavoo.aura.steam

import android.util.Log
import com.dejavoo.aura.apps.common.core.PrintFiles
import java.io.File

class WebSteamParametersUpdate(
        private val downloadPath: String,
        private val outputPath: String
) : ParametersUpdate {

    override fun update(tpn: String) {
        if (tpn.isEmpty()) {
            throw IllegalArgumentException("TPN must be non empty")
        }

        prepareDirectory(downloadPath)
        val files = SteamParametersCompleteUnpacker(downloadPath).execute(
                SteamTpnParametersDownload(tpn).stream()
        )
        PrintFiles(downloadPath).print()

        prepareDirectory(outputPath)
        SteamFilesSaveAsStream(outputPath).execute(files)
        PrintFiles(outputPath).print()

        removeDirectory(downloadPath)


        Log.e("Web steam update", "Downloaded files to: '$downloadPath', \nsaved output to: '$outputPath'")

    }

    private fun prepareDirectory(path: String) {
        File(path).deleteRecursively().also {
            Log.i("SteamParametersUpdate", "Clearing $path finished = $it")
        }
        File(path).mkdirs().also {
            Log.i("SteamParametersUpdate", "Create directory $path finished = $it")
        }
    }

    private fun removeDirectory(path: String) {
        File(path).deleteRecursively().also {
            Log.i("SteamParametersUpdate", "Clearing $path finished = $it")
        }
    }
}