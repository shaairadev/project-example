package com.dejavoo.aura.steam

import android.util.Log
import java.io.File

interface SteamFilesSave {
    fun execute(files: List<SteamFile>)
}

class SteamFilesSaveAsStream(
        private val savePath: String
) : SteamFilesSave {

    override fun execute(files: List<SteamFile>) {
        files.forEach(::saveFile)
    }

    private fun saveFile(file: SteamFile) {
        try {
            Log.i("SteamFilesSave", "Copying ${file.filePath} to $savePath/${file.fileName}")

            if (!File(file.filePath).exists()) {
                Log.e("SteamFilesSave", "Not found file: ${file.filePath}")
            }
            
            OutputToFile(file.stream(), "$savePath/${file.fileName}").toFile()
        } catch (e: Exception) {
            Log.e("SteamFilesSave", "Copying ${file.filePath} failed", e)
//            throw e
        }
    }
}