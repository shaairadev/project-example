package com.dejavoo.aura.steam.archives

import com.dejavoo.aura.steam.SteamFile

interface SteamArchive {
    fun unpack(): List<SteamFile>
}