package com.dejavoo.aura.steam.archives

import com.dejavoo.aura.steam.LocalFileSteamFile
import com.dejavoo.aura.steam.SteamFile
import com.dejavoo.aura.steam.archives.jtar.TarEntry

import com.dejavoo.aura.steam.archives.jtar.TarInputStream
import java.io.*

class TarSteamArchive(
        private val inputStream: InputStream,
        private val outputDirectory: String
): SteamArchive {
    override fun unpack(): List<SteamFile> {
        val tarInputStream = TarInputStream(inputStream)

        val files = mutableListOf<SteamFile>()

        var entry: TarEntry? = tarInputStream.nextEntry

        while (entry != null) {
            var count: Int
            val data = ByteArray(2048)

            val filePath = "$outputDirectory/${entry.name}"
            val fos = FileOutputStream(filePath)
            val dest = BufferedOutputStream(fos)

            while (true) {
                count = tarInputStream.read(data)

                if (count == -1) {
                    break
                }

                dest.write(data, 0, count)
            }
            dest.flush()
            dest.close()

            files.add(LocalFileSteamFile(filePath))
            entry = tarInputStream.nextEntry
        }

        tarInputStream.close()

        return files.toList()
    }

}